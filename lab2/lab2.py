import math
m= input('Do you wanna encryt or decrypt message?(e/d) ')
if m == 'e':
    mode = 'encrypt'
elif m == 'd':
    mode = 'decrypt'
else:
    print('Incorrect command')
    
message = input('Message to {}: '.format(mode))#Your message

key = int(math.sqrt(len(message))) #cipher key

LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' #alphabet
translated = ''
# capitalize the string in message
message = message.upper()
i=0
for symbol in message: 
    if symbol in LETTERS: 
        # get the encrypted (or decrypted) number for this symbol
        num = LETTERS.find(symbol) # get the number of the symbol
        i+=1
        if mode=='encrypt': 
            if i%2 == 0:
                num = num + key
            elif i%2 != 0:
                num = num - key
        elif mode =='decrypt':
             if i%2 == 0:
                num = num - key
             elif i%2!= 0:
                num = num + key
        # wrap-around if num > length of LETTERS or less than 0
        if num >= len(LETTERS):
            num = num - len(LETTERS)
        elif num < 0:
            num = num + len(LETTERS)
        # add encrypted/decrypted number's symbol at the end of translated
        translated = translated + LETTERS[num]
    else:
        # just add the symbol without encrypting/decrypting
        translated = translated + symbol

print(translated)#print result message
